use std::fs::{self, OpenOptions};
use std::io::{Read, Seek, SeekFrom, Write};
use std::{collections::BTreeSet, path::PathBuf};

use clap::Parser;
use unicode_segmentation::UnicodeSegmentation;

#[derive(Parser)]
#[clap(
    author = "Denis P.",
    version,
    about = "Extract all words from a file and put them into a destination file. If the destination file already exists, then simply add the words of the source file."
)]
struct App {
    #[clap(value_parser)]
    source: PathBuf,
    #[clap(value_parser)]
    destination: PathBuf,
}

fn main() -> std::io::Result<()> {
    let app = App::parse();

    let file_content = fs::read_to_string(app.source)?;

    let mut words: BTreeSet<String> = file_content
        .unicode_words()
        .filter(|&wrd| !(wrd.chars().any(|c| c.is_numeric())))
        .map(|wrd| wrd.to_lowercase())
        .collect();
    drop(file_content);

    let mut destination = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open(app.destination)?;

    let mut destination_content = String::new();
    destination.read_to_string(&mut destination_content)?;

    words.extend(destination_content.lines().map(|wrd| wrd.to_owned()));

    // to prevent problems with empty string
    if words.contains(&String::new()) {
        words.remove(&String::new());
    }

    destination.seek(SeekFrom::Start(0))?;

    for word in words.iter() {
        destination.write_all(word.as_bytes())?;
        destination.write_all(b"\n")?;
    }

    Ok(())
}