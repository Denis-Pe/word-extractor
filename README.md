# word-extractor

Command-line utility to extract words from a file.

## Usage

`word-extractor input.txt output.txt`

This will extract all words from `input.txt` and put them in `output.txt` in alphabetical order. However, if `output.txt` already exists, what will happen is that it will "merge" the two files, outputting all words that `input.txt` has *combined* with all of the words that `output.txt` already has, ditching duplicates.

## To-do

- [ ] Look for optimizations to perform.
- [ ] Add support for blob pattern.
- [ ] Add support for multiple inputs in the style of `word-extractor foo.txt bar.txt baz.txt -o output.txt`.